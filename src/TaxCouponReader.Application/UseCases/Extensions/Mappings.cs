﻿using TaxCouponReader.Application.UseCases.Models;

namespace TaxCouponReader.Application.UseCases.Extensions
{
    public static class Mappings
    {
        public static GetTaxCouponInfoOutput ToGetTaxCouponInfoOutput(this Domain.Entities.TaxCoupon taxCoupon)
        {
            return new GetTaxCouponInfoOutput()
            {
                Amount = taxCoupon.Amount,
                CompanyName = taxCoupon.CompanyName,
                CNPJ = taxCoupon.CNPJ,
                Products = taxCoupon.Products,
                RecognizedBy = taxCoupon.RecognizedBy,
                IsValid = taxCoupon.IsValid()
            };
        }
    }
}

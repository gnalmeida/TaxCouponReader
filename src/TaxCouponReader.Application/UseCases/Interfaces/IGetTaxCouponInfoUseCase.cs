﻿using MediatR;
using TaxCouponReader.Application.UseCases.Models;

namespace TaxCouponReader.Application.UseCases.Interfaces
{
    public interface IGetTaxCouponInfoUseCase : IRequestHandler<GetTaxCouponInfoInput, GetTaxCouponInfoOutput>
    {
    }
}

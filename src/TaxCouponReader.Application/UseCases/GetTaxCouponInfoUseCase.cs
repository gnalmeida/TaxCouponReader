﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TaxCouponReader.Application.UseCases.Extensions;
using TaxCouponReader.Application.UseCases.Interfaces;
using TaxCouponReader.Application.UseCases.Models;
using TaxCouponReader.Domain.Entities;
using TaxCouponReader.Domain.Interfaces;

namespace TaxCouponReader.Application.UseCases
{
    public class GetTaxCouponInfoUseCase : IGetTaxCouponInfoUseCase
    {
        private readonly IOcrService _ocrService;
        private readonly IFormRecognizerService _formRecognizerService;

        public GetTaxCouponInfoUseCase(IOcrService ocrService, IFormRecognizerService formRecognizerService)
        {
            _ocrService = ocrService;
            _formRecognizerService = formRecognizerService;
        }

        public async Task<GetTaxCouponInfoOutput> Handle(GetTaxCouponInfoInput request, CancellationToken cancellationToken)
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "tempImages");
            var fileNames = new List<string>();

            try
            {
                foreach (var item in request.ImageFiles)
                {
                    string newFileName = $"{Guid.NewGuid()}_{item.Key}";
                    using (Stream fileStream = new FileStream(Path.Combine(filePath, newFileName), FileMode.Create))
                    {
                        await item.Value.CopyToAsync(fileStream, cancellationToken);
                        fileNames.Add(newFileName);
                    }
                }

                var taxCoupons = await Task.WhenAll(fileNames.Select(fileName => GetTaxCouponByTesseract(filePath, fileName, cancellationToken)));

                var taxCoupon = taxCoupons.FirstOrDefault(t => t.IsValid());
                if (taxCoupon != null)
                {
                    return taxCoupon.ToGetTaxCouponInfoOutput();
                }

                foreach (var fileName in fileNames)
                {
                    taxCoupon = await _formRecognizerService.GetImageText(filePath, fileName); ;
                    if (taxCoupon != null && taxCoupon.IsValidCompanyName() && taxCoupon.IsValidAmount() && taxCoupon.IsValidProducts())
                    {
                        return taxCoupon.ToGetTaxCouponInfoOutput();
                    }
                }

                return taxCoupons.FirstOrDefault()?.ToGetTaxCouponInfoOutput();
            }
            finally
            {
                foreach (var fileName in fileNames)
                {
                    File.Delete(Path.Combine(filePath, fileName));
                }
            }
        }

        private async Task<TaxCoupon> GetTaxCouponByTesseract(string filePath, string newFileName, CancellationToken cancellationToken)
        {
            TaxCoupon taxCoupon = null;
            var taxCoupons = new List<TaxCoupon>();

            taxCoupon = await _ocrService.GetImageText(filePath, newFileName);
            if (taxCoupon.IsValid())
            {
                return taxCoupon;
            }

            taxCoupons.Add(taxCoupon);

            taxCoupon = await _ocrService.GetImageText(filePath, newFileName, softImage: true);
            if (taxCoupon.IsValid())
            {
                return taxCoupon;
            }

            taxCoupons.Add(taxCoupon);

            var thresholds = new int[] { 190, 180, 170, 120, 110 };
            foreach (var threshold in thresholds)
            {
                taxCoupon = await _ocrService.GetImageText(filePath, newFileName, threshold: threshold);
                if (taxCoupon.IsValid())
                {
                    return taxCoupon;
                }

                taxCoupons.Add(taxCoupon);
            }

            taxCoupon = taxCoupons.LastOrDefault(t => t.IsValidCNPJ());

            var companyName = taxCoupon?.CompanyName ?? taxCoupons.FirstOrDefault(t => t.IsValidCompanyName() && !string.IsNullOrWhiteSpace(t.CNPJ))?.CompanyName ?? "";
            var cnpj = taxCoupon?.CNPJ ?? "";
            var amount = taxCoupons.Where(t => t.IsValidAmount()).Select(t => t.Amount).Max();
            var products = taxCoupons.Where(t => t.IsValidProducts()).OrderByDescending(t => t.Products.Count).FirstOrDefault()?.Products ?? new List<string>();

            return new TaxCoupon(companyName, cnpj, amount, products, "Tesseract");
        }
    }
}

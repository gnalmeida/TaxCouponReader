﻿using MediatR;
using System.Collections.Generic;
using System.IO;

namespace TaxCouponReader.Application.UseCases.Models
{
    public class GetTaxCouponInfoInput : IRequest<GetTaxCouponInfoOutput>
    {
        public GetTaxCouponInfoInput()
        {
            ImageFiles = new Dictionary<string, Stream>();
        }

        public Dictionary<string, Stream> ImageFiles { get; set; }
    }
}

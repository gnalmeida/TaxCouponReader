﻿using System.Collections.Generic;

namespace TaxCouponReader.Application.UseCases.Models
{
    public class GetTaxCouponInfoOutput
    {
        public string CompanyName { get; set; }
        public string Amount { get; set; }
        public List<string> Products { get; set; }
        public string CNPJ { get; set; }
        public bool IsValid { get; set; }
        public string RecognizedBy { get; set; }
    }
}

﻿using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TaxCouponReader.Domain.Interfaces;

namespace TaxCouponReader.Data
{
    public class ComputerVisionService : IComputerVisionService
    {
        private readonly string _key;
        private readonly string _endpoint;

        public ComputerVisionService(IConfiguration configuration)
        {
            _key = configuration["ComputerVision:Key"];
            _endpoint = configuration["ComputerVision:Endpoint"];
        }

        public async Task<string> GetImageText(string filePath, string fileName)
        {
            var credentials = new ApiKeyServiceClientCredentials(_key);

            using (var client = new ComputerVisionClient(credentials) { Endpoint = _endpoint })
            {
                using (Stream imageFileStream = File.OpenRead(Path.Combine(filePath, fileName)))
                {
                    var ocrResult = await client.RecognizePrintedTextInStreamAsync(false, imageFileStream, OcrLanguages.Pt);

                    var strBuilder = new StringBuilder();
                    foreach (var region in ocrResult.Regions)
                    {
                        foreach (var line in region.Lines)
                        {
                            strBuilder.AppendLine(" ");
                            foreach (var word in line.Words)
                            {
                                strBuilder.Append($"{word.Text} ");
                            }
                        }
                    }

                    return strBuilder.ToString();
                }
            }
        }
    }
}

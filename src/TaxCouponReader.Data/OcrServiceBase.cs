﻿using AForge.Imaging;
using AForge.Imaging.Filters;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.IO;
using Tesseract;
using Image = System.Drawing.Image;

namespace TaxCouponReader.Data
{
    public class OcrServiceBase
    {
        public bool SofImage { get; set; }
        public int? Threshold { get; private set; }
        public int? Interations { get; set; }
        public decimal? CropWidth { get; private set; } = 1;
        public decimal? CropHeight { get; private set; } = 1;

        public void SetSoftImage(bool softImage)
        {
            SofImage = softImage;
        }

        public void SetThreshold(int threshold)
        {
            Threshold = threshold;
        }

        public void SetInterations(int interations)
        {
            Interations = interations;
        }

        public void SetCropWidth(decimal cropHeight)
        {
            CropWidth = cropHeight;
        }

        public void SetCropHeight(decimal cropHeight)
        {
            CropHeight = cropHeight;
        }

        private Image<Emgu.CV.Structure.Gray, Byte> ToImageCV(byte[] bytes)
        {
            using (var bmp = new System.Drawing.Bitmap(new System.IO.MemoryStream(bytes)))
            {
                return bmp.ToImage<Emgu.CV.Structure.Gray, Byte>();
            }
        }

        private Image<Emgu.CV.Structure.Gray, Byte> ToImageCV(string imagePath)
        {
            return new Image<Emgu.CV.Structure.Gray, Byte>(imagePath);
        }

        private Bitmap Skew(Bitmap documentImage)
        {
            // create instance of skew checker
            DocumentSkewChecker skewChecker = new DocumentSkewChecker();
            // get documents skew angle
            double angle = skewChecker.GetSkewAngle(documentImage);
            // create rotation filter
            RotateBilinear rotationFilter = new RotateBilinear(-angle);
            rotationFilter.FillColor = Color.White;
            // rotate image applying the filter
            Bitmap rotatedImage = rotationFilter.Apply(documentImage);

            return rotatedImage;
        }

        private Image<Gray, byte> ToSoftImage(Image<Gray, byte> imageCV)
        {
            return imageCV.SmoothBlur(2, 2);
        }

        private Image<Gray, byte> ToThresholdedImage(Image<Gray, byte> imageCV, int threshold)
        {
            return imageCV.ThresholdBinary(new Gray(threshold), new Gray(255));
        }

        private Image<Gray, byte> ToDilatedImage(Image<Gray, byte> thresholdedImage, int interations)
        {
            return thresholdedImage.Erode(interations);
        }

        private string GetTextFromMemory(byte[] bytes)
        {
            using (var engine = new TesseractEngine(@"./tessdata", "por", EngineMode.Default))
            {
                using (var img = Pix.LoadFromMemory(bytes))
                {
                    using (var page = engine.Process(img))
                    {
                        return page.GetText();
                    }
                }
            }
        }

        private string GetTextFromFile(string fileName)
        {
            using (var engine = new TesseractEngine(@"./tessdata", "por", EngineMode.Default))
            {
                using (var img = Pix.LoadFromFile(fileName))
                {
                    using (var page = engine.Process(img))
                    {
                        return page.GetText();
                    }
                }
            }
        }

        private static byte[] ImageToByte(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                return stream.ToArray();
            }
        }

        private Mat Crop(Mat input, Rectangle crop_region)
        {
            Image<Bgr, Byte> buffer_im = input.ToImage<Bgr, Byte>();
            buffer_im.ROI = crop_region;

            Image<Bgr, Byte> cropped_im = buffer_im.Copy();

            return cropped_im.Mat;
        }

        public string GetImageText(string filePath, string fileName)
        {
            string fullPath = Path.Combine(filePath, fileName);
            string ROI_FullPath = Path.Combine(filePath, $"{Guid.NewGuid()}_{fileName}");
            Bitmap bmp;

            var imageCV = ToImageCV(fullPath);

            if (SofImage)
            {
                imageCV = ToSoftImage(imageCV);
            }

            if (Threshold > 0)
            {
                imageCV = ToThresholdedImage(imageCV, Threshold.GetValueOrDefault());

                if (Interations > 0)
                {
                    imageCV = ToDilatedImage(imageCV, Interations.GetValueOrDefault());
                }
            }

            if (CropHeight < 1 || CropWidth < 1)
            {
                var cropImage = Crop(imageCV.Mat, new Rectangle() { Height = (int)(imageCV.Height * CropHeight), Width = (int)(imageCV.Width * CropWidth) });
                bmp = cropImage.ToBitmap();
                imageCV = bmp.ToImage<Gray, Byte>();
                bmp.Save(ROI_FullPath);
            }

            bmp = imageCV.ToBitmap();
            bmp = Skew(bmp);
            bmp.Save(ROI_FullPath);

            //var text = GetTextFromMemory(ImageToByte(bmp));
            var text = GetTextFromFile(ROI_FullPath);

            File.Delete(ROI_FullPath);

            return text;
        }
    }
}

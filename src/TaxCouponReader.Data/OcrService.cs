﻿using System.Threading.Tasks;
using TaxCouponReader.Domain.Entities;
using TaxCouponReader.Domain.Extensions;
using TaxCouponReader.Domain.Interfaces;

namespace TaxCouponReader.Data
{
    public class OcrService : OcrServiceBase, IOcrService
    {
        public async Task<TaxCoupon> GetImageText(string filePath, string fileName, bool softImage = false, int threshold = 0, int interations = 0)
        {
            if (softImage)
            {
                base.SetSoftImage(softImage);
            }

            if (threshold > 0)
            {
                base.SetThreshold(threshold);

                if (interations > 0)
                {
                    base.SetInterations(interations);
                }
            }

            var coupomText = await Task.FromResult(base.GetImageText(filePath, fileName));

            return coupomText.ToTaxCoupon();
        }
    }
}

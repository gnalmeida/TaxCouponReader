﻿using Azure.AI.FormRecognizer;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TaxCouponReader.Domain.Entities;
using TaxCouponReader.Domain.Interfaces;

namespace TaxCouponReader.Data
{
    public class FormRecognizerService : IFormRecognizerService
    {
        private readonly string _key;
        private readonly string _endpoint;
        private readonly bool _enable;

        public FormRecognizerService(IConfiguration configuration)
        {
            _key = configuration["FormRecognizer:Key"];
            _endpoint = configuration["FormRecognizer:Endpoint"];
            _enable = !string.IsNullOrWhiteSpace(_key) && !string.IsNullOrWhiteSpace(_endpoint);
        }

        public async Task<TaxCoupon> GetImageText(string filePath, string fileName)
        {
            if (!_enable) return null;

            using (Stream imageFileStream = File.OpenRead(Path.Combine(filePath, fileName)))
            {
                var formRecognizerClient = new FormRecognizerClient(new System.Uri(_endpoint), new Azure.AzureKeyCredential(_key));
                var recognizeReceiptsOperation = await formRecognizerClient.StartRecognizeReceiptsAsync(imageFileStream).WaitForCompletionAsync();

                var merchant = recognizeReceiptsOperation.Value[0].Fields.ContainsKey("MerchantName") ? recognizeReceiptsOperation.Value[0].Fields["MerchantName"] : null;
                var total = recognizeReceiptsOperation.Value[0].Fields.ContainsKey("Total") ? recognizeReceiptsOperation.Value[0].Fields["Total"] : null;
                var items = recognizeReceiptsOperation.Value[0].Fields.ContainsKey("Items") ? recognizeReceiptsOperation.Value[0].Fields["Items"] : null;

                var productList = new List<string>();
                if (items != null)
                {
                    foreach (var item in items.Value.AsList())
                    {
                        productList.Add(item.Value.AsDictionary()["Name"].ValueData.Text);
                    }
                }

                return new TaxCoupon(merchant.ValueData?.Text ?? "", "", total?.ValueData?.Text ?? "0", productList, "FormRecognizer");
            }
        }
    }
}

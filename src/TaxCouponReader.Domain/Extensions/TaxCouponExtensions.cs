﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TaxCouponReader.Domain.Entities;

namespace TaxCouponReader.Domain.Extensions
{
    public static class TaxCouponExtensions
    {
        public static TaxCoupon ToTaxCoupon(this string text)
        {
            var fullTextArray = text.Split("\n").Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

            var ocrSelectorConfig = new OcrSelectorConfig();

            var companyName = GetCompanyNameText(fullTextArray, ocrSelectorConfig);
            var cNPJ = GetCNPJText(fullTextArray, ocrSelectorConfig);
            var products = GetProductList(fullTextArray, ocrSelectorConfig);
            var amount = GetAmountText(fullTextArray, ocrSelectorConfig);

            return new TaxCoupon(companyName, cNPJ, amount, products, "Tesseract");
        }

        private static List<string> GetProductList(string[] fullTextArray, OcrSelectorConfig ocrSelectorConfig)
        {
            var flgItem = false;
            var productList = new List<string>();
            var textArray = fullTextArray;

            for (int i = 0; i < textArray.Length; i++)
            {
                var item = textArray[i];

                if (item.Contains(ocrSelectorConfig.ProductStartText, System.StringComparison.CurrentCultureIgnoreCase) ||
                    item.Contains(ocrSelectorConfig.ProductStartText2, System.StringComparison.CurrentCultureIgnoreCase))
                {
                    flgItem = true;
                    continue;
                }

                if (item.Contains(ocrSelectorConfig.ProductEndText, System.StringComparison.CurrentCultureIgnoreCase) ||
                    item.Contains(ocrSelectorConfig.AmountText, System.StringComparison.CurrentCultureIgnoreCase))
                {
                    break;
                }

                if (flgItem)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        var itemWords = item.Split(' ');
                        StringBuilder sb = new StringBuilder();

                        for (int itemWordIndex = 0; itemWordIndex < itemWords.Length; itemWordIndex++)
                        {
                            var word = itemWords[itemWordIndex];

                            //is not item number
                            if (itemWordIndex == 0 && !Regex.IsMatch(word, @"^[0-9]+$"))
                            //if (itemWordIndex == 0 && word.Length >= 4)
                            {
                                break;
                            }

                            //only letters
                            //if (Regex.IsMatch(word, @"^[a-zA-Z]+$"))
                            if (!Regex.IsMatch(word, @"^\d$") && !Regex.IsMatch(word, @"^[0-9]+$"))
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(' ');
                                }

                                sb.Append(word);
                            }
                        }

                        if (sb.Length > 0)
                        {
                            productList.Add(sb.ToString());
                        }
                    }
                }
            }

            return productList;
        }

        private static string GetAmountText(string[] fullTextArray, OcrSelectorConfig _ocrSelectorConfig)
        {
            string amount = "0";

            var textArray = fullTextArray;

            for (int i = 0; i < textArray.Length; i++)
            {
                var item = textArray[i];

                if (item.Contains(_ocrSelectorConfig.AmountText, System.StringComparison.CurrentCultureIgnoreCase))
                {
                    amount = Regex.Replace(item, @"[a-zA-Z$| ]", string.Empty);
                    break;
                }
            }

            decimal.TryParse(amount, out var value);

            return value.ToString();
        }

        private static string GetCompanyNameText(string[] fullTextArray, OcrSelectorConfig _ocrSelectorConfig)
        {
            var textArray = fullTextArray;

            return fullTextArray.Length > 0 ? Regex.Replace(textArray[_ocrSelectorConfig.CompanyNameIndex], @"[^a-zA-Z ]", string.Empty).Trim() : "";
        }

        private static string GetCNPJText(string[] fullTextArray, OcrSelectorConfig _ocrSelectorConfig)
        {
            string cnpj = "";

            var textArray = fullTextArray;

            for (int i = 0; i < textArray.Length; i++)
            {
                var item = textArray[i];

                if (item.Contains(_ocrSelectorConfig.CNPJText, System.StringComparison.CurrentCultureIgnoreCase))
                {
                    cnpj = Regex.Replace(item, @"[a-zA-Z $/.:,-]", string.Empty);
                    cnpj = cnpj.Length > 14 ? cnpj.Substring(0, 14) : cnpj.Substring(0, cnpj.Length);
                    break;
                }
            }

            return cnpj;
        }
    }
}

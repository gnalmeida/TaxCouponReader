﻿using System.Threading.Tasks;
using TaxCouponReader.Domain.Entities;

namespace TaxCouponReader.Domain.Interfaces
{
    public interface IFormRecognizerService
    {
        Task<TaxCoupon> GetImageText(string filePath, string fileName);
    }
}

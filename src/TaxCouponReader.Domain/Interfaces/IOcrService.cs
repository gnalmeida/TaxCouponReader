﻿using System.Threading.Tasks;
using TaxCouponReader.Domain.Entities;

namespace TaxCouponReader.Domain.Interfaces
{
    public interface IOcrService
    {
        Task<TaxCoupon> GetImageText(string filePath, string fileName, bool softImage = false, int threshold = 0, int interations = 0);
    }
}

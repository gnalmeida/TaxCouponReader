﻿using System.Threading.Tasks;

namespace TaxCouponReader.Domain.Interfaces
{
    public interface IComputerVisionService
    {
        Task<string> GetImageText(string filePath, string fileName);
    }
}

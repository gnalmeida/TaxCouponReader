﻿namespace TaxCouponReader.Domain.Entities
{
    public class OcrSelectorConfig
    {
        public int CompanyNameIndex { get; set; } = 0;
        public string CNPJText { get; set; } = "CNP";
        public string AmountText { get; set; } = "TOTAL R$";
        public string ProductStartText { get; set; } = "ITEM";
        public string ProductStartText2 { get; set; } = "QTD";
        public string ProductEndText { get; set; } = "TOTAL";
    }
}

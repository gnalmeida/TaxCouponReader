﻿using System.Collections.Generic;
using TaxCouponReader.Domain.Extensions;

namespace TaxCouponReader.Domain.Entities
{
    public class TaxCoupon
    {
        public TaxCoupon(string companyName, string cNPJ, string amount, List<string> products, string recognizedBy)
        {
            CompanyName = companyName;
            CNPJ = cNPJ;
            Amount = amount;
            Products = products;
            RecognizedBy = recognizedBy;
        }

        public string CompanyName { get; private set; }
        public string CNPJ { get; private set; }
        public string Amount { get; private set; }
        public List<string> Products { get; private set; }
        public string RecognizedBy { get; private set; }

        public int Precision => GetPrecision();
        public bool IsValidCompanyName() => !string.IsNullOrWhiteSpace(CompanyName);
        public bool IsValidAmount() => Amount != "0" && decimal.TryParse(Amount, out _);
        public bool IsValidCNPJ() => !string.IsNullOrWhiteSpace(CNPJ) && long.TryParse(CNPJ, out _) && CNPJ.IsCnpj();
        public bool IsValidProducts() => Products.Count > 0;

        public bool IsValid()
        {
            return IsValidAmount() &&
                IsValidCNPJ() &&
                IsValidProducts() &&
                IsValidCompanyName();
        }

        private int GetPrecision()
        {
            var precision = 0;

            if (!string.IsNullOrWhiteSpace(CNPJ) && long.TryParse(CNPJ, out _) && CNPJ.IsCnpj())
            {
                precision += 4;
            }

            if (Amount != "0" && decimal.TryParse(Amount, out _))
            {
                precision += 3;
            }

            if (Products.Count > 0)
            {
                precision += 2;
            }

            if (!string.IsNullOrWhiteSpace(CompanyName))
            {
                precision += 1;
            }

            return precision;
        }
    }
}

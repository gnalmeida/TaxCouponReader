﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using TaxCouponReader.Application.UseCases.Models;

namespace TesteOCR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaxCouponController : ControllerBase
    {
        private readonly ILogger<TaxCouponController> logger;
        private readonly IMediator _mediator;

        public TaxCouponController(IMediator mediator, ILogger<TaxCouponController> logger)
        {
            _mediator = mediator;
            this.logger = logger;
        }


        [HttpPost]
        public async Task<IActionResult> Post(IFormFile[] files)
        {
            var input = new GetTaxCouponInfoInput();
            foreach (var file in files)
            {
                input.ImageFiles.Add(file.FileName, file.OpenReadStream());
            }

            var output = await _mediator.Send(input);

            return Ok(output);
        }
    }
}

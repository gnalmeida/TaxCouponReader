﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using TaxCouponReader.Application.UseCases;
using TaxCouponReader.Application.UseCases.Interfaces;
using TaxCouponReader.Data;
using TaxCouponReader.Domain.Interfaces;

namespace TaxCouponReader.API
{
    public class IocManager
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //Application
            services.AddScoped<IGetTaxCouponInfoUseCase, GetTaxCouponInfoUseCase>();

            //Repository

            //Database

            //Infra
            services.AddScoped<IOcrService, OcrService>();
            services.AddScoped<IComputerVisionService, ComputerVisionService>();
            services.AddScoped<IFormRecognizerService, FormRecognizerService>();

            //MediatR
            services.AddMediatR(typeof(IGetTaxCouponInfoUseCase).GetTypeInfo().Assembly);
        }
    }
}

# Introdução 
O Tax Coupon Reader é uma API que reconhece imagens de cupons fiscais.

# Tecnologias utilizadas:
- Tesseract;
- AForge;
- AForge.Imaging;
- Emgu.CV.Bitmap;
- Azure.AI.FormRecognizer;

# Arquitetura do Projeto:

- Clean Architecture;

# Como rodar:
O SDK e as ferramentas mais recentes podem ser baixados em https://dot.net/core.

## Comandos para executar o projeto:
- dotnet restore
- dotnet build
- dotnet run --project src/TaxCouponReader.Api/TaxCouponReader.API.csproj
